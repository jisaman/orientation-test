// static folder to serve files from
var gameDirectory = __dirname + "/public";

//Our Session secret
var sessionSecret = process.env.SESSION_SECRET || "SOME RANDOM STRING";

var express = require('express');

var app = express.createServer(
		//express.logger(),
		express.static( gameDirectory ),
		express.cookieParser(),
		express.bodyParser(),           // parse post data of the body
		express.session({ secret: sessionSecret })
);

//Views and Routes
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(app.router);

app.configure( 'development', function()
{
	app.use( express.errorHandler( { dumpExceptions: true, showStack: true } ) );
} );

app.configure( 'prodution', function()
{
	app.use( express.errorHandler() );
} );

var port = process.env.PORT || 5000;
app.listen( port, function()
{
	console.log( "Express server listening on port %d in %s mode", app.address().port, app.settings.env );
} );

app.get( "/", function( req, res )
{
	console.log( "Serving index ejs" );
	res.render( "index", { } );
});