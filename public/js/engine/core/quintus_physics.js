
Quintus.Physics = function(Q) {

  var B2d = Q.B2d = {
      World: Box2D.Dynamics.b2World,
      Vec: Box2D.Common.Math.b2Vec2,
      BodyDef: Box2D.Dynamics.b2BodyDef,
      Body: Box2D.Dynamics.b2Body,
      FixtureDef: Box2D.Dynamics.b2FixtureDef,
      Fixture: Box2D.Dynamics.b2Fixture,
      RevoluteJointDef: Box2D.Dynamics.Joints.b2RevoluteJointDef,
      RevoluteJoint: Box2D.Dynamics.Joints.b2RevoluteJoint,
      PolygonShape: Box2D.Collision.Shapes.b2PolygonShape,
      CircleShape: Box2D.Collision.Shapes.b2CircleShape,
      Listener:  Box2D.Dynamics.b2ContactListener,
      AABB: Box2D.Collision.b2AABB
    };

  var defOpts = Q.PhysicsDefaults = {
    gravityX: 0,
    gravityY: 10,
    scale: 30,
    velocityIterations: 10,
    positionIterations: 5
  };

  Q.component('world',{
    added: function() {
      this.opts = _(defOpts).clone();
      this._gravity = new B2d.Vec(this.opts.gravityX,
                                 this.opts.gravityY);

      this._world = new B2d.World(this._gravity, true);
      
      _.bindAll(this,"beginContact","endContact","postSolve");
  
      this._listener = new B2d.Listener();
      this._listener.BeginContact = this.beginContact;
      this._listener.EndContact = this.endContact;
      this._listener.PostSolve = this.postSolve;

      this._world.SetContactListener(this._listener);
      
      this.col = {};

      this.scale = this.opts.scale;
      this.entity.on('step',this,'boxStep');
      
      var debugDraw = new Box2D.Dynamics.b2DebugDraw();
		debugDraw.SetSprite(Q.ctx);
		debugDraw.SetDrawScale(this.scale);
		debugDraw.SetFillAlpha(0.5);
		debugDraw.SetLineThickness(1.0);
		debugDraw.SetFlags(Box2D.Dynamics.b2DebugDraw.e_shapeBit | Box2D.Dynamics.b2DebugDraw.e_jointBit);
		this._world.SetDebugDraw(debugDraw);
    },

    setCollisionData: function(contact,impulse) {
      var spriteA = contact.GetFixtureA().GetBody().GetUserData(),
          spriteB = contact.GetFixtureB().GetBody().GetUserData();
       
      this.col["a"] = spriteA;
      this.col["b"] = spriteB;
      this.col["impulse"] = impulse;
      this.col["sprite"] = null;
    },

    beginContact: function(contact) {
      this.setCollisionData(contact,null);

      this.col.a.trigger("contact",this.col.b);
      this.col.b.trigger("contact",this.col.a);
      this.entity.trigger("contact",this.col);
    },

    endContact: function(contact) {
      this.setCollisionData(contact,null);

      this.col.a.trigger("endContact",this.col.b);
      this.col.b.trigger("endContact",this.col.a);
      this.entity.trigger("endContact",this.col);
    },

    postSolve: function(contact, impulse) {
      this.setCollisionData(contact,impulse);

      this.col["sprite"] = this.col.b;
      this.col.a.trigger("impulse",this.col);

      this.col["sprite"] = this.col.a;
      this.col.b.trigger("impulse",this.col);

      this.entity.trigger("impulse",this.col);
    },

    createBody: function(def) {
      return this._world.CreateBody(def);
    },

    destroyBody: function(body) {
      return this._world.DestroyBody(body);
    },

    boxStep: function(dt) {
      if(dt > 1/20) { dt = 1/20; }
      this._world.Step(dt, 
                      this.opts.velocityIterations,
                      this.opts.positionIterations);
    },
    
    queryAABB : function( callback, aabb )
    {
    	this._world.QueryAABB( callback, aabb );
    },
    
    drawDebug : function()
    {
    	this._world.DrawDebugData();
    }
  });



  var entityDefaults = Q.PhysicsEntityDefaults = {
    density: 1,
    friction: 1,
    restitution: .1
  };

  Q.component('physics',{
    added: function() {
      if(this.entity.parent) {
        this.inserted();
      } else {
        this.entity.on('inserted',this,'inserted');
      }
      this.entity.on('step',this,'step');
      this.entity.on('removed',this,'removed');
    },

    position: function(x,y) {
      var stage = this.entity.parent;
      this._body.SetAwake(true);
      this._body.SetPosition(new B2d.Vec(x / stage.world.scale,
                                         y / stage.world.scale));
    },
    
    setPositionAndAngle: function( x, y, angleToSet )
    {
    	this.position( x, y );
    	this.angle( angleToSet );
    },

    angle: function(angle) {
      this._body.SetAngle(angle / 180 * Math.PI);
    },
    
    setAwake: function( awake )
    {
    	this._body.SetAwake( awake );
    },

    velocity: function(x,y) {
      var stage = this.entity.parent;
      this._body.SetAwake(true);
      this._body.SetLinearVelocity(new B2d.Vec(x / stage.world.scale,
                                               y / stage.world.scale));
    },
    
    applyImpulse : function( force, point )
    {
    	var forceVec = new B2d.Vec( force.x, force.y );
    	var pointVec;
    	
    	if( point != null )
    	{
    		pointVec = new B2d.Vec( point.x, point.y );
    	}
    	else
		{
    		pointVec = this._body.GetWorldCenter();
		}
    	
    	this._body.ApplyImpulse( forceVec, pointVec );
    },
 
    inserted: function() {
      var entity = this.entity,
          stage = entity.stage,
          scale = stage.world.scale,
          p = entity.p,
          ops = entityDefaults,
          def = this._def = new B2d.BodyDef,
          fixtures = p.fixtures,
          fixtureDefs = this._fixtures = [],
          fixtureDef;
      
      def.position.x = p.x / scale;
      def.position.y = p.y / scale;
      if( p.allowSleep !== null )
      {
        def.allowSleep = p.allowSleep;
      }
      def.type = p.type == 'static' ? 
                 B2d.Body.b2_staticBody :
                 B2d.Body.b2_dynamicBody;
      def.active = true;
      
      this._body = stage.world.createBody(def); 
      this._body.SetAngle( ( p.rotation || 0 ) / 180 * Math.PI );
      this._body.SetBullet( p.bullet );
      
      var i = 0;
      var max = fixtures.length;
      
      for( i; i < max; i++ )
      {
          fixtureDef = new B2d.FixtureDef;
          fixtureDef.density = fixtures[ i ].density || p.density || ops.density;
          fixtureDef.friction = fixtures[ i ].friction || p.friction || ops.friction;
          fixtureDef.restitution = fixtures[ i ].restitution || p.restitution || ops.restitution;
          switch(fixtures[ i ].shape) {
              case "box":
                fixtureDef.shape = new B2d.PolygonShape;
                fixtureDef.shape.SetAsBox(fixtures[ i ].w/scale, fixtures[ i ].h/scale);
                break;
	          case "orientedBox":
	            fixtureDef.shape = new B2d.PolygonShape;
	            fixtureDef.shape.SetAsOrientedBox(fixtures[ i ].w/scale, fixtures[ i ].h/scale, new B2d.Vec( fixtures[ i ].x/scale, fixtures[ i ].y/scale ));
	            break;
	          case "circle":
	            fixtureDef.shape = new B2d.CircleShape(fixtures[ i ].r/scale);
	            break;
	          case "polygon":
	            fixtureDef.shape = new B2d.PolygonShape;
	            var pointsObj = _.map(fixtures[ i ].points,function(pt) {
	              return new B2d.Vec( pt.x / scale, pt.y / scale );
	            });
	            fixtureDef.shape.SetAsArray(pointsObj, fixtures[ i ].points.length);
	            break;
	        }
          
          this._fixtures.push( this._body.CreateFixture(fixtureDef) );
          fixtureDefs.push( fixtureDef );
      }
      
      this._body.SetUserData(entity);
      this._body._bbid = p.id;
    },

    removed: function() {
      var entity = this.entity,
          stage = entity.parent;
      stage.world.destroyBody(this._body);
    },

    step: function() {
      var p = this.entity.p,
          stage = this.entity.stage,
          pos = this._body.GetPosition(),
          angle = this._body.GetAngle() * Math.PI / 180;

      p.x = pos.x * stage.world.scale;
      p.y = pos.y * stage.world.scale;
      p.angle = angle / Math.PI * 180;

    }
  });

  

};


