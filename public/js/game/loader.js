Modernizr.load( [ {
	load: [
		"js/libs/jquery.min.js",
		"js/libs/underscore.js",
		"js/engine/core/Quintus.js",
		"js/engine/core/quintus_input.js",
		"js/engine/core/quintus_scenes.js",
		"js/engine/core/quintus_physics.js",
		"js/engine/core/quintus_sprites.js",
		"js/engine/core/quintus_anim.js",
		"js/game/main.js",
		"js/libs/Box2dWeb-2.1.a.3.js"
	],
	complete: function() {
		GAME.init();
	}
} ] );