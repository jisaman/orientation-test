Quintus.TrebuchetLevelParser = function(Q) {
	console.log( "Starting level parser" );
	
	Q.BoundingBox = Q.Sprite.extend(
	{
		init : function( props )
		{
			this._super(
			{
				fixtures: [
					{ shape: "orientedBox", w: 1500, h: 50, x: 0, y: 750 },
					{ shape: "orientedBox", w: 1500, h: 50, x: 0, y: -1950 },
					{ shape: "orientedBox", w: 50, h: 1400, x: -1450, y: -600 },
					{ shape: "orientedBox", w: 50, h: 1400, x: 1450, y: -600 }
				],
				x: 2560 / 2,
				y: 720 / 2,
				type: "static"
			} );
			this.bind( "impulse", this.onImpulse );
			this.add( "physics" );
		},
		
		onImpulse : function( collisionData )
		{
			if( collisionData.a instanceof Q.BoundingBox )
			{
				Q.stage().remove( collisionData.b );
			}
			else if( collisionData.b instanceof Q.BoundingBox )
			{
				Q.stage().remove( collisionData.a );
			}
		}
	});
	
	Q.DestructibleObject = Q.Sprite.extend(
	{
		init : function( props )
		{
			this._super( _(
			{
				damage: 0,
				damageLevel: 0,
				destroyed: false
			} ).extend( props || {} ) );
			this.bind( "impulse", this.onImpulse );
			this.bind( "destructionAnimComplete", this, this.onDestructionAnimComplete );
			this.add( "physics" );
		},
		
		onDestructionAnimComplete : function()
		{
			Q.stage().remove( this );
		},
		
		applyDamage : function( impulseDamage )
		{
			this.p.damage += Math.round( impulseDamage );
			
			// TODO: adjust texture to match damage
			var damagePercent = this.p.damage / this.p.damageThreshold;
			
			if( !this.p.destroyed )
			{
				if( damagePercent >= 1 )
				{
					this.p.destroyed = true;
					
					if( this.p.scoreValue )
					{
						this.trigger( "destroyed", this.p.scoreValue );
					}
					
					if( this.play != null )
					{
						this.play( "onDestroy" );
					}
				}
				else if( damagePercent > 0.7 )
				{
					this.play( "destructionLevel2" );
				}
				else if( damagePercent > 0.4 )
				{
					this.play( "destructionLevel1" );
				}
			}
		},
		
		onImpulse : function( collisionData )
		{
			var impulseDamage = collisionData.impulse.normalImpulses[ 0 ] + collisionData.impulse.normalImpulses[ 1 ];
			
			if( impulseDamage >= 4 )
			{
				this.applyDamage( impulseDamage );
			}
		}
	} );
	
	Q.ExplodingObject = Q.DestructibleObject.extend(
	{
		init : function( props )
		{
			this._super( _(
			{
				explosionForce: 20,
				explosionRadius: 150
			} ).extend( props || {} ) );
		},
		
		applyDamage : function( impulseDamage )
		{
			this._super( impulseDamage );
			if( this.p.damage >= this.p.damageThreshold )
			{
				this.explode();
			}
		},
		
		explode : function()
		{
			// TODO: Write explosion code
			var radius,
				force,
				worldCenter,
				aabb,
				i,
				fixtureCount,
				hitBodyPos,
				hitVector,
				radDist,
				hitForce,
				appForce,
				totalForce,
				explosionQueryArray = [];
			
			radius = this.p.explosionRadius / Q.stage().world.scale;
			force = this.p.explosionForce;
			worldCenter = this.physics._body.GetWorldCenter();
			aabb = new Q.B2d.AABB();
			aabb.lowerBound.Set( ( worldCenter.x - radius ), ( worldCenter.y - radius ) );
			aabb.upperBound.Set( ( worldCenter.x + radius ), ( worldCenter.y + radius ) );
			
			Q.stage().world.queryAABB( function( fixture )
			{
				explosionQueryArray.push( fixture.GetBody() );
				return true;
			}, aabb );
			
			i = 0;
			fixtureCount = explosionQueryArray.length;
			
			for( i; i < fixtureCount; i++ )
			{
				hitBodyPos = explosionQueryArray[ i ].GetWorldCenter();
				hitVector = new Q.B2d.Vec( hitBodyPos.x - worldCenter.x, hitBodyPos.y - worldCenter.y );
				radDist = hitVector.Normalize();
				if( explosionQueryArray[ i ].GetType() === 2 && ( radDist <= radius ) )
				{
					if( explosionQueryArray[ i ] != this.physics._body )
					{
						hitForce = ( ( radius - radDist ) / radius ) * force;
						appForce = new Q.B2d.Vec( hitVector.x * hitForce, hitVector.y * hitForce );
						if( explosionQueryArray[ i ].GetUserData() != null && explosionQueryArray[ i ].GetUserData().hasOwnProperty( "damage" ) )
						{
							totalForce = appForce.x + appForce.y;
							if( totalForce < 0 )
							{
								totalForce = totalForce * ( -1 );
							}
							explosionQueryArray[ i ].GetUserData().applyDamage( totalForce );
						}
						explosionQueryArray[ i ].ApplyImpulse( appForce, worldCenter );
					}
				}
			}
		}
	} );
	
	Q.StoneObject = Q.DestructibleObject.extend(
	{
		init : function( props )
		{
			this._super( _(
			{
				density: 0.8,
				friction: 0.9,
				restitution: 0.1
			} ).extend( props || {} ) );
		}
	} );
	
	Q.StonePillar = Q.StoneObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "orientedBox", w: 10, h: 5, x: 0, y: -45 },
					{ shape: "orientedBox", w: 10, h: 5, x: 0, y: 45 },
					{ shape: "box", w: 7.5, h: 45, x: 0, y: 45 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 100,
				sheet: "StonePillar",
				sprite: "stone-pillar",
				scoreValue: 50
			} );
			this.add( "animation" );
		}
	} );
	
	Q.StoneBoulder = Q.StoneObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "circle", r: 10 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 20,
				bullet: true,
				sheet: "StoneBoulder",
				sprite: "stone-boulder"
			} );
			this.add( "animation" );
		}
	} );
	
	Q.StoneBlock = Q.StoneObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 10, h: 25 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 40,
				sheet: "StoneBlock",
				sprite: "stone-block",
				scoreValue: 25
			} );
			this.add( "animation" );
		}
	} );
	
	Q.StoneBeam = Q.StoneObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 5, h: 50 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 50,
				sheet: "StoneBeam",
				sprite: "stone-beam",
				scoreValue: 30
			} );
			this.add( "animation" );
		}
	} );
	
	Q.SteelObject = Q.Sprite.extend(
	{
		init : function( props )
		{
			this._super( _(
			{
				density: 1.2,
				friction: 0.8,
				restitution: 0.1
			} ).extend( props || {} ) );
			this.add( "physics" );
		}
	} );
	
	Q.SteelBeam = Q.SteelObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 5, h: 50 }
				],
				x: x,
				y: y,
				rotation: rotation,
				asset: "steel-beam.png"
			} );
		}
	} );
	
	Q.CannonBall = Q.SteelObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "circle", r: 10 }
				],
				x: x,
				y: y,
				rotation: rotation,
				bullet: true,
				asset: "cannon-ball.png"
			} );
		}
	} );
	
	Q.Bomb = Q.ExplodingObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "orientedBox", w: 3, h: 3, x: 0, y: -10 },
					{ shape: "circle", r: 10 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 1,
				explosionForce: 20,
				explosionRadius: 150,
				bullet: true,
				sheet: "Bomb",
				sprite: "bomb"
			} );
			this.add( "animation" );
		}
	} );
	
	Q.ExplodingCrate = Q.ExplodingObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 15, h: 15 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 5,
				explosionForce: 30,
				explosionRadius: 200,
				sheet: "ExplodingCrate",
				sprite: "exploding-crate",
				scoreValue: 25
			} );
			this.add( "animation" );
		}
	} );
	
	Q.GroundObject = Q.Sprite.extend(
	{
		init : function( props )
		{
			this._super( _(
			{
				density: 1,
				friction: 0.8,
				restitution: 0.1,
				type: "static"
			} ).extend( props || {} ) );
			this.add( "physics" );
		}
	} );
	
	Q.Ground1 = Q.GroundObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 1280, h: 50 }
				],
				x: x,
				y: y,
				rotation: rotation,
				asset: "ground-1.png"
			} );
		}
	} );
	
	Q.Hill1 = Q.GroundObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "polygon", points: [ { x: -100, y: -20 }, { x: 100, y: -20 }, { x: 150, y: 20 }, { x: -150, y: 20 } ] }
				],
				x: x,
				y: y,
				rotation: rotation,
				asset: "hill-1.png"
			} );
		}
	} );
	
	Q.WoodenObject = Q.DestructibleObject.extend(
	{
		init : function( props )
		{
			this._super( _(
			{
				density: 0.5,
				friction: 0.7,
				restitution: 0.25
			} ).extend( props || {} ) );
		}
	} );
	
	Q.WoodenBeam = Q.WoodenObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 5, h: 50 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 100,
				sheet: "WoodenBeam",
				sprite: "wooden-beam",
				scoreValue: 15
			} );
			this.add( "animation" );
		}
	} );
	
	Q.WoodenBox = Q.WoodenObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 15, h: 15 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 40,
				sheet: "WoodenBox",
				sprite: "wooden-box",
				scoreValue: 15
			} );
			this.add( "animation" );
		}
	} );
	
	Q.WoodenBlock = Q.WoodenObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 10, h: 25 }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 35,
				sheet: "WoodenBlock",
				sprite: "wooden-block",
				scoreValue: 25
			} );
			this.add( "animation" );
		}
	} );
	
	Q.WoodenRoof = Q.WoodenObject.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "polygon", points: [ { x: 0, y: -30 }, { x: 60, y: 30 }, { x: -60, y: 30 } ] }
				],
				x: x,
				y: y,
				rotation: rotation,
				damageThreshold: 50,
				sheet: "WoodenRoof",
				sprite: "wooden-roof",
				scoreValue: 20
			} );
			this.add( "animation" );
		}
	} );
	
	Q.RopeLink = Q.Sprite.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 8, h: 2 }
				],
				x: x,
				y: y,
				rotation: rotation,
				asset: "rope-link.png",
				density: 1,
				friction: 0.1,
				restitution: 0.1
			} );
			this.add( "physics" );
		}
	} );
	
	Q.TrebuchetWeight = Q.Sprite.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 30, h: 30 },
					{ shape: "orientedBox", w: 7.5, h: 18, x: 0, y: -35 },
				],
				x: x,
				y: y,
				rotation: rotation,
				asset: "trebuchet-weight.png",
				density: 10,
				friction: 0.3,
				restitution: 0.1
			} );
			this.add( "physics" );
		}
	} );
	
	Q.TrebuchetArm = Q.Sprite.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 150, h: 7.5 }
				],
				x: x,
				y: y,
				rotation: rotation,
				asset: "trebuchet-arm.png",
				density: 1,
				friction: 0.3,
				restitution: 0.1
			} );
			this.add( "physics" );
		}
	} );
	
	Q.TrebuchetBase = Q.Sprite.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "box", w: 2, h: 2 }
				],
				x: x,
				y: y,
				rotation: rotation,
				type: "static",
				asset: "trebuchet-base.png",
				density: 1,
				friction: 0.3,
				restitution: 0.1
			} );
			this.add( "physics" );
		}
	} );
	
	Q.Trebuchet = Q.Sprite.extend(
	{
		init : function( x, y, rotation, stage )
		{
			// setup trebuchet
			// rope
			var i = 0,
				max = 6,
				rope,
				joint,
				ropeBodies = [],
				ropeObjects = [];
			
			for( i; i < max; i++ )
			{
				rope = stage.insert( new Q.RopeLink( x - 140 + ( i * 16 ), y + 145 ) );
				rope.physics.setAwake( false );
				rope.add( "physics" );
				
				ropeBodies.push( rope.physics._body );
				ropeObjects.push( rope );
				
				if( i > 0 )
				{
					joint = new Q.B2d.RevoluteJointDef();
					joint.Initialize( rope.physics._body, ropeBodies[ ropeBodies.length - 2 ], new Q.B2d.Vec( rope.physics._body.GetPosition().x - ( 5 / stage.world.scale ), rope.physics._body.GetPosition().y ) );
					joint.collideConnected = false;
					stage.world._world.CreateJoint( joint );
				}
			}
			
			var weight = stage.insert( new Q.TrebuchetWeight( x + 60, y - 15 ) );
			weight.physics._body.SetFixedRotation( true );
			weight.physics.setAwake( false );
			
			var arm = stage.insert( new Q.TrebuchetArm( x - 40, y + 40, 315 ) );
			arm.physics.setAwake( false );
			
			var base = stage.insert( new Q.TrebuchetBase( x, y ) );
			
			// arm to base joint
			var armToBaseJointDef = new Q.B2d.RevoluteJointDef();
			armToBaseJointDef.Initialize( base.physics._body, arm.physics._body, base.physics._body.GetPosition() );
			armToBaseJointDef.collideConnected = false;
			stage.world._world.CreateJoint( armToBaseJointDef );
			
			// weight to arm joint
			var weightToArmJointDef = new Q.B2d.RevoluteJointDef();
			weightToArmJointDef.collideConnected = false;
			weightToArmJointDef.Initialize( weight.physics._body, arm.physics._body, new Q.B2d.Vec( ( x + 60 ) / stage.world.scale, ( y - 60 ) / stage.world.scale ) );
			stage.world._world.CreateJoint( weightToArmJointDef );
			
			// arm to rope joint
			var armToRopeJointDef = new Q.B2d.RevoluteJointDef();
			armToRopeJointDef.Initialize( arm.physics._body, ropeBodies[ 0 ], new Q.B2d.Vec( ropeBodies[ 0 ].GetPosition().x - ( 8 / stage.world.scale ), ( y + 145 ) / stage.world.scale ) );
			armToRopeJointDef.collideConnected = false;
			stage.world._world.CreateJoint( armToRopeJointDef );
			
			this._super( {
				x : x,
				y: y,
				rotation: rotation,
				trebuchetArm : arm,
				trebuchetWeight : weight,
				ropeObjects: ropeObjects,
				stage: stage,
				projectile: null,
				projectileReleased: true,
				trebuchetReleased: true,
				slingJoint: null
			} );
		},
		
		loadProjectile : function( projectile )
		{
			if( this.p.projectile != null && !this.p.projectileReleased )
			{
				this.p.stage.forceRemove( this.p.projectile );
			}
			if( this.p.slingJoint != null )
			{
				this.p.stage.world._world.DestroyJoint( this.p.slingJoint );
			}
			
			this.p.projectile = projectile;
			
			this.p.trebuchetArm.physics.setPositionAndAngle( this.p.x - 40 , this.p.y + 40, 315 );
			this.p.trebuchetArm.physics._body.SetLinearVelocity( new Q.B2d.Vec( 0, 0 ) );
			this.p.trebuchetArm.physics._body.SetAngularVelocity( 0 );
			this.p.trebuchetArm.physics.setAwake( false );
			
			this.p.trebuchetWeight.physics.setPositionAndAngle( this.p.x + 60, this.p.y - 15, 0 );
			this.p.trebuchetWeight.physics._body.SetLinearVelocity( new Q.B2d.Vec( 0, 0 ) );
			this.p.trebuchetWeight.physics._body.SetAngularVelocity( 0 );
			this.p.trebuchetWeight.physics.setAwake( false );
			
			var i = 0;
			var max = this.p.ropeObjects.length;
			
			for( i; i < max; i ++ )
			{
				this.p.ropeObjects[ i ].physics.setPositionAndAngle( this.p.x - 140 + ( i * 16 ), this.p.y + 145, 0 );
				this.p.ropeObjects[ i ].physics._body.SetLinearVelocity( new Q.B2d.Vec( 0, 0 ) );
				this.p.ropeObjects[ i ].physics._body.SetAngularVelocity( 0 );
				this.p.ropeObjects[ i ].physics.setAwake( false );
			}
			
			projectile.physics.position( this.p.x - 50, this.p.y + 145 );
			projectile.physics.setAwake( false );
			
			var lastRopeIndex = this.p.ropeObjects.length - 1;
			var ropeToProjectileJointDef = new Q.B2d.RevoluteJointDef();
			ropeToProjectileJointDef.Initialize( projectile.physics._body, this.p.ropeObjects[ lastRopeIndex ].physics._body, new Q.B2d.Vec( this.p.ropeObjects[ lastRopeIndex ].physics._body.GetPosition().x, this.p.ropeObjects[ lastRopeIndex ].physics._body.GetPosition().y ) );
			ropeToProjectileJointDef.collideConnected = false;
			this.p.slingJoint = this.p.stage.world._world.CreateJoint( ropeToProjectileJointDef );
			
			this.p.projectileReleased = false;
			this.p.trebuchetReleased = false;
		},
		
		releaseTrebuchet : function( )
		{
			this.p.trebuchetWeight.physics.setAwake( true );
			this.p.trebuchetReleased = true;
		},
		
		releaseProjectile : function( )
		{
			if( this.p.slingJoint != null )
			{
				this.p.stage.world._world.DestroyJoint( this.p.slingJoint );
			}
			this.p.projectileReleased = true;
		}
	} );
	
	Q.register( "levelParser" ,
	{
		parseLevelData : function( levelXML, stage )
		{
			stage.setTriesLeft( levelXML.getElementsByTagName( "tries" )[ 0 ].childNodes[ 0 ].nodeValue );
			
			var levelObjectArray = levelXML.getElementsByTagName( "levelObject" );
			var i = 0;
			var max = levelObjectArray.length;
			var levelObjectNode;
			var classType;
			
			for( i; i < max; i++ )
			{
				levelObjectNode = levelObjectArray[ i ];
				
				classType = this.getLevelObject( levelObjectNode.getElementsByTagName( "type" )[ 0 ].childNodes[ 0 ].nodeValue );
				
				// assume this is a stage object
				stage.insert( new classType( 
						levelObjectNode.getElementsByTagName( "location" )[ 0 ].getElementsByTagName( "x" )[ 0 ].childNodes[ 0 ].nodeValue,
						levelObjectNode.getElementsByTagName( "location" )[ 0 ].getElementsByTagName( "y" )[ 0 ].childNodes[ 0 ].nodeValue,
						levelObjectNode.getElementsByTagName( "rotation" )[ 0 ].childNodes[ 0 ].nodeValue
				) );
			}
		},
		
		getLevelObject : function( type )
		{
			return Q[ type.split( "::" )[ 1 ] ];
		}
	} );
};