var GAME = GAME || {};

GAME.init = function()
{
	var Q = window.Q = Quintus().include( "Input,Sprites,Scenes,Physics,Anim" ).setup( "quintus", { maximize: true } );

	Q.Ball = Q.Sprite.extend(
	{
		init : function( x, y, rotation )
		{
			this._super( {
				fixtures: [
					{ shape: "circle", r: 20 }
				],
				x: x,
				y: y,
				rotation: rotation,
			} );
			this.add( "physics" );
		}
	} );

	Q.BoundingWall = Q.Sprite.extend(
	{
		init : function( props )
		{
			this._super( _( props ).defaults ( {
				fixtures: [
					{ shape: "box", x: 0, y: 0, w: props.w, h: props.h }
				],
				type: "static",
				allowSleep: false
			} ) );
			this.add( "physics" );
		}
	} );

	function rotateContainer()
	{
		Q.el.style.webkitTransform = "rotate(" + -1 * window.orientation + "deg)";
	}
	rotateContainer();
	window.addEventListener( "orientationchange", rotateContainer );

	Q.scene( "test", new Q.Scene( function( stage )
	{
		if( window.DeviceOrientationEvent )
		{
			window.addEventListener( "deviceorientation", function( e )
			{
				var eventData = e,
					tiltLR = eventData.gamma,
					tiltFB = eventData.beta,
					direction = eventData.alpha;

				var leanAngle = tiltLR * Math.PI / 180,
					tiltAngle = tiltFB * Math.PI / 180,
					gravityX = 50 * Math.sin( leanAngle ),
					gravityY = 50 * Math.sin( tiltAngle );

				stage.world._world.m_gravity.x = gravityX;
				stage.world._world.m_gravity.y = gravityY;
			}, true );
		}

		stage.add( "world" );
		stage.add( "viewport" );

		var canvas = Q.el;
		var width = canvas.width;
		var height = canvas.height;

		stage.insert( new Q.BoundingWall( { x: 5, y: 300, w: 10, h: height } ) );
		stage.insert( new Q.BoundingWall( { x: width - 5, y: 300, w: 10, h: height } ) );
		stage.insert( new Q.BoundingWall( { x: 200, y: 5, w: width, h: 10 } ) );
		stage.insert( new Q.BoundingWall( { x: 200, y: height - 5, w: width, h: 10 } ) );

		var xSpacing = 50;
		var ySpacing = 50;
		var rowIndex = 1;
		var columnIndex = 1;
		for( var i = 0; i < 10; i++ )
		{
			if( columnIndex > 5 )
			{
				rowIndex++;
				columnIndex= 1;
			}
			stage.insert( new Q.Ball( xSpacing * columnIndex, ( rowIndex * ySpacing ), 0 ) );
			columnIndex++;
		}

		/*stage.insert( new Q.Ball( 300, 100, 0 ) );
		stage.insert( new Q.Ball( 100, 100, 0 ) );
		stage.insert( new Q.Ball( 100, 300, 0 ) );
		stage.insert( new Q.Ball( 300, 300, 0 ) );*/

	} ) );
	
	Q.stageScene( "test" );
}